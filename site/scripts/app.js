// ThreeJS variables
var scene, camera, renderer, controller;
var CANVAS_WIDTH = 800, CANVAS_HEIGHT = 600,
	CANVAS_RATIO = CANVAS_WIDTH / CANVAS_HEIGHT;

var example, config, gui;

window.onload = start;

function start() {
	example = new Example();
	config = new Config();
	example.init(config);
	example.animate();

	gui = new dat.GUI();
	initGUI();
}

function initGUI() {
	var c; // controller pointer

	var model_list = {
		Bunny: "bunny.obj",
		Eight: "eight.obj",
		Hand: "hand.obj",
		Horse: "horse.obj",
		Gargoyle: "gargoyle.obj",
		Sculpture: "sculpture.obj",
		Topology: "topology.obj",
		Torus: "torus.obj",
		Debug: "debug.obj"
	}

	c = gui.add(example, 'autoAdjustCamera').name("Auto-Adjust");
	c = gui.add(config, 'model_name', model_list).name("Model");
	c.onChange(updateModel);

	var renderFolder = gui.addFolder('Render Options');
		c = renderFolder.addColor(config, 'clear_color').name("Clear Color");
		c.onChange(updateClearColor);

	var materialFolder = gui.addFolder('Material Options');
		c = materialFolder.add(config, 'material_type', {Basic: 'basic', Lambert: 'lambert', Phong: 'phong'}).name("Type");
		c.onChange(updateMaterialType);
		c = materialFolder.addColor(config, 'material_color').name("Color");
		c.onChange(updateMaterialColor);

	var lightFolder = gui.addFolder('Light Options');
		c = lightFolder.add(config, 'light_x', -1000.0, 1000.0).name("X");
		c.onChange(updateLightX);
		c = lightFolder.add(config, 'light_y', -1000.0, 1000.0).name("Y");
		c.onChange(updateLightY);
		c = lightFolder.add(config, 'light_z', -1000.0, 1000.0).name("Z");
		c.onChange(updateLightZ);
		c = lightFolder.addColor(config, 'light_color').name("Color");
		c.onChange(updateLightColor);
	
	var propFolder = gui.addFolder('Geometric Properties');
		propFolder.add(example.model_properties.count, 'faces').name("Faces");
		propFolder.add(example.model_properties.count, 'edges').name("Edges");
		propFolder.add(example.model_properties.count, 'vertices').name("Vertices");
		propFolder.add(example.model_properties.geometry, 'eulerNumber').name("Euler Number");
		propFolder.add(example.model_properties.geometry, 'genus').name("Genus");
		propFolder.add(example.model_properties.geometry, 'gaussianCurvature').name("Gaussian Curvature");
}

function updateCameraView() {
	exmaple.autoAdjustCamera();
}

function updateModel(model_name) {
	example.config.model_name = model_name;
	example.config.model_path = example.config.model_dir + "/" + model_name;

	example.refreshModel();

	gui.__folders['Geometric Properties'].updateDisplay()
}

function updateClearColor(clear_color) {
	example.config.clear_color = clear_color;
	
	example.refreshRenderer();
}

function updateMaterialType(material_type) {
	example.config.material_type = material_type;

	example.refreshMaterial();
}

function updateMaterialColor(material_color) {
	example.config.material_color = material_color;

	example.refreshMaterial();
}

function updateLightX(light_x) {
	example.config.light_x = light_x;

	example.refreshLight();
}

function updateLightY(light_y) {
	example.config.light_y = light_y;

	example.refreshLight();
}

function updateLightZ(light_z) {
	example.config.light_z = light_z;

	example.refreshLight();
}

function updateLightColor(light_color) {
	example.config.light_color = light_color;

	example.refreshLight();
}
