# CAP 4710 Homework Assignment
*Author*: Daniel A. Herrera
*Date* March 25, 2017

## Project Description

This repo contains my submission for my CAP 4710 homework assignments.
The projects is intended to provide an object viewer powered by [three.js](https://threejs.org/)
that will allow for rotation, scaling, and translation of the object, as well
as texture mapping and lighting simulation.

Information about the geometry of the model will also be displayed, to be
verified by the user. The calculated value for the model's genus will be
displayed alongside the sum of vertices' Gaussian curvatures.

## Hosting this Repo

This repo's code is currently being hosted on my personal server over [here](http://cinnamon.shew.io:5000).
Over there you can see the finished project running. Since this repo is public,
you can clone it and try hosting it yourself. I've made this website with the
intent of it being hosted as a static website on an [Nginx](https://www.nginx.com/resources/wiki/) server like I'm
doing currently. To run this repo yourself, I would suggest setting up an Ubuntu
server with Nginx and then cloning this repo to that server. I've included a
script that publishes the site to some directory that can be configured from
within the script. I recommend modifying the script to work for your server's
configuration; I've commented the script enough for most to be able to read
through it and be able to understand what has to be done to host this repo.

The pages for this site can be viewed offline, but will probably not work, as
scripts and files are expected to be in certain locations within a server, as such
their addresses may not line up with the local filesystem. There is that, as well
as the fact that some browsers prevent loading of files from the local filesystem
within scripts for security reasons.

## Example Functionality

### Main Project Screen:

![Home Screen](img/home-screen.png)

To view what configuration will yield the results in the following images,
look towards the right side. All model, rendering, material, and lighting
options currently implemented.

These are examples of some of the configurable components:

### Auto-Adjust

Because the models available have various sizes, switching between them
might leave the camera at an awkward position. After switching from the
`Hand` model to the `Eight` model, the view looks like this:

![Auto-Adjust 1](img/auto-adjust-1.png)

After clicking `Auto-Adjust` at the top of the menu box, the camera will
adjust to the following view:

![Auto Adjust 2](img/auto-adjust-2.png)

### Material Options

There are three different materials that can be used to create the models:

- `Basic`: Unaffected by light
- `Lambert`: Affected by light, but has low reflectivity
- `Phong`: Affected by light, with high reflectivity

I've added the ability to change the color used as a base for the material.
Here are some examples of the same model with different materials:

![Material Type 1](img/material-type-1.png)

![Material Type 2](img/material-type-2.png)

![Material Type 3](img/material-type-3.png)

### Light Options

I've included the ability to move around the single light source within the
scene and change it's color as well. Currently, the light being used is a
`PointLight`, but later on, more light types may be accessible.

Here is an example of the `Torus` model being lit from different positions,
causing different portions of it to be lit.

![Light Properties 1](img/light-properties-1.png)

![Light Properties 2](img/light-properties-2.png)

### Geometric Properties

At the bottom of the GUI, a folder has been added that contains the calculated
geometrical properties for the loaded mesh.

![Geometrical Properties](img/geo-properties.png)

_Some values are erroneously calculated. I couldn't figure out what happens
with some of the loaded models. :(_

## Results Summary

| Category | Items |
| --- | --- |
| **Done**		| |
|			| Set up libraries for use |
| 		| Create base webpage |
| 		| Set up rendering pipeline |
|			| Add `obj` file uploading |
|			| Implement configurable translation |
|			| Implement configurable scaling |
|			| Implement configurable rotation |
|			| Add `.obj` to half-edge parser |
|			| Implement Euler number calculations |
|			| Implement Gaussian curvature calculations |
|			| Add Half-Edge data structure |
| **To-Do**		| |
|			| Implement configurable lighting sources | 
|			| Implement texure mapping |
|			| Implement configurable lighting sources |
|			| Add screenshots and explain feature list |
|			| Make website prettier to look at |
|			| Add resizeable render size |
